package com.setel.ui

import android.os.Bundle
import com.google.android.material.navigation.NavigationView
import androidx.test.espresso.idling.CountingIdlingResource
import androidx.core.view.GravityCompat
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.setel.R
import com.setel.ui.geofence.GeofenceFragment
import kotlinx.android.synthetic.main.activity_main.*

import kotlinx.android.synthetic.main.app_bar_main.*

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    val espressoTestIdlingResource = CountingIdlingResource("Network_Call");
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        initDrawer()

        //renderDefaultFragment
        renderFragment(R.id.nav_transaction_history)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initDrawer(){
        val toggle = ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navView.setNavigationItemSelectedListener(this);


    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        val id = item.getItemId();

        renderFragment(id);

        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private fun renderFragment(id: Int){
        var fragment : GeofenceFragment? = null;

        if (id == R.id.nav_transaction_history) {
            // Handle transaction history
            fragment =  GeofenceFragment.newInstance()
            val fragmentManager = supportFragmentManager
            fragmentManager.beginTransaction().replace(R.id.main_container, fragment).commit()
        }
    }

    /**
     *
     * @return MainActvity's idling resource for Espresso testing
     */
    fun getEspressoIdlingResourceForMainActivity() : CountingIdlingResource {
        return espressoTestIdlingResource;
    }
}
