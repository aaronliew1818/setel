package com.setel.ui.geofence

import android.location.Location
import com.google.android.gms.location.GeofencingClient
import com.google.android.gms.location.GeofencingRequest
import com.google.android.gms.maps.GoogleMap
import com.setel.constant.Constants

/**
 * Created by aaronliew on 02/03/2018.
 */
interface GeoFenceViewPresenter {
    fun setView(geofenceView: GeofenceView)
    fun initGeofence(mGeofencingClient: GeofencingClient, currentLocation: Location)
    fun getGeofencingRequest() : GeofencingRequest
    fun populateGeofenceList(radius: Float = Constants.GEOFENCE_RADIUS_IN_METERS)
}