package com.setel.ui.geofence;

import android.app.Activity
import android.location.Location
import com.google.android.gms.location.Geofence
import com.google.android.gms.location.GeofencingClient
import com.google.android.gms.location.GeofencingRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.GoogleMap
import com.setel.constant.Constants

/**
 * Created by bot on 27/01/2018.
 */

class GeoFenceViewPresenterImpl : GeoFenceViewPresenter {

    private lateinit var geofenceView: GeofenceView

    /**
     * Provides access to the Geofencing API.
     */
    private lateinit var mGeofencingClient: GeofencingClient

    /**
     * The list of geofences used in this sample.
     */
    private val mGeofenceList = arrayListOf<Geofence>()

    private lateinit var currentLocation: Location

    override fun setView(geofenceView: GeofenceView) {
        this.geofenceView = geofenceView
    }

    override fun initGeofence(mGeofencingClient: GeofencingClient, currentLocation: Location) {
        this.currentLocation = currentLocation
        this.mGeofencingClient = mGeofencingClient
        populateGeofenceList()
    }

    /**
     * Builds and returns a GeofencingRequest. Specifies the list of geofences to be monitored.
     * Also specifies how the geofence notifications are initially triggered.
     */
    override fun getGeofencingRequest() : GeofencingRequest {
        val builder =  GeofencingRequest.Builder()

        // The INITIAL_TRIGGER_ENTER flag indicates that geofencing service should trigger a
        // GEOFENCE_TRANSITION_ENTER notification when the geofence is added and if the device
        // is already inside that geofence.
        builder.setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER);

        // Add the geofences to be monitored by geofencing service.
        builder.addGeofences(mGeofenceList);

        // Return a GeofencingRequest.
        return builder.build()
    }

    /**
     * This sample hard codes geofence data. A real app might dynamically create geofences based on
     * the user's location.
     */
    override fun populateGeofenceList(radius: Float) {
        mGeofenceList.clear()
        val info = Constants.BAY_AREA_LANDMARKS.entries.iterator().next()
        mGeofenceList.add(Geofence.Builder()
                // Set the request ID of the geofence. This is a string to identify this
                // geofence.
                .setRequestId(info.key)

                // Set the circular region of this geofence.
                .setCircularRegion(
                        currentLocation.latitude + Constants.GEOFENCE_DISTANCE,
                        currentLocation.longitude + Constants.GEOFENCE_DISTANCE,
                        radius
                )

                // Set the expiration duration of the geofence. This geofence gets automatically
                // removed after this period of time.
                .setExpirationDuration(Constants.GEOFENCE_EXPIRATION_IN_MILLISECONDS)

                // Set the transition types of interest. Alerts are only generated for these
                // transition. We track entry and exit transitions in this sample.
                .setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER or Geofence.GEOFENCE_TRANSITION_EXIT)

                // Create the geofence.
                .build())

    }
}
