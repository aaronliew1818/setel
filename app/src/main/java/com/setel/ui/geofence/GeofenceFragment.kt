package com.setel.ui.geofence

import android.Manifest;
import android.app.Activity
import android.app.PendingIntent;
import android.content.Context
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color
import android.location.Criteria
import android.location.Location
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.Uri;
import android.net.wifi.WifiManager
import android.os.Bundle;
import android.os.VibrationEffect
import android.os.Vibrator
import android.preference.PreferenceManager;
import android.provider.Settings;
import com.google.android.material.snackbar.Snackbar;
import androidx.core.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater
import android.view.View;
import android.view.ViewGroup
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.Fragment
import com.github.pwittchen.reactivenetwork.library.rx2.ConnectivityPredicate
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.Circle
import com.google.android.gms.maps.model.CircleOptions
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.setel.BaseApplication
import com.setel.BuildConfig
import com.setel.R
import com.setel.constant.Constants
import com.setel.di.component.DaggerGeofenceComponent
import com.setel.di.component.GeofenceComponent
import com.setel.di.module.GeoFenceModule
import com.setel.model.GeofenceEvent
import com.setel.ui.geofence.broadcastreceiver.GeofenceBroadcastReceiver
import com.setel.ui.geofence.broadcastreceiver.GeofenceErrorMessages
import com.xw.repo.BubbleSeekBar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_geofence.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

import javax.inject.Inject

/**
 * Demonstrates how to create and remove geofences using the GeofencingApi. Uses an IntentService
 * to monitor geofence transitions and creates notifications whenever a device enters or exits
 * a geofence.
 * <p>
 * This sample requires a device's Location settings to be turned on. It also requires
 * the ACCESS_FINE_LOCATION permission, as specified in AndroidManifest.xml.
 * <p>
 */
class GeofenceFragment : Fragment(), OnCompleteListener<Void>, OnMapReadyCallback, GeofenceView {

    companion object{
        private val TAG = GeofenceFragment::class.java.simpleName
        fun newInstance() : GeofenceFragment = GeofenceFragment();
        private val REQUEST_PERMISSIONS_REQUEST_CODE = 34
    }

    private lateinit var geofenceComponent : GeofenceComponent

    private lateinit var geoFenceView: View

    /**
     * Tracks whether the user requested to add or remove geofences, or to do neither.
     */
    enum class PendingGeofenceTask {
        ADD, REMOVE, NONE
    }

    /**
     * Provides access to the Geofencing API.
     */
    private lateinit var mGeofencingClient: GeofencingClient

    /**
     * Used when requesting to add or remove geofences.
     * Initially set the PendingIntent used in addGeofences() and removeGeofences() to null.
     */
    private var mGeofencePendingIntent: PendingIntent? = null

    private var mPendingGeofenceTask = PendingGeofenceTask.NONE

    @Inject
    lateinit var presenter : GeoFenceViewPresenter

    private lateinit var mMap: GoogleMap

    private val circleOptionsList = arrayListOf<CircleOptions>()

    var circle: Circle? = null

    private val compositeDisposable =  CompositeDisposable();

    fun getGeofenceComponent() : GeofenceComponent {
        geofenceComponent = DaggerGeofenceComponent.builder()
                .geoFenceModule(GeoFenceModule())
                .appComponent(BaseApplication.get(getActivity() as Context).getComponent())
                .build()

        return geofenceComponent;
    }

    private var newSSID = ""
    private var oldSSID = ""

    private var triggeringGeofencesIdString = ""
    private var geofenceTransition = -1
    @SuppressWarnings("MissingPermission")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getGeofenceComponent().inject(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_geofence, container, false);
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.setView(this)
        geoFenceView = view
        initWifiAndMaps()
    }

    private fun initWifiAndMaps(){
        attachMapFragmentToView()

        val disposable = ReactiveNetwork
                .observeNetworkConnectivity(context)
                .subscribeOn(Schedulers.io())
                .filter(ConnectivityPredicate.hasState(NetworkInfo.State.CONNECTED))
                .filter(ConnectivityPredicate.hasType(ConnectivityManager.TYPE_WIFI))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (!checkPermissions()) {
                        requestPermissions()
                    } else {
                        if (it.type() == ConnectivityManager.TYPE_WIFI){
                            val wifiManager = activity?.applicationContext?.getSystemService (Context.WIFI_SERVICE) as WifiManager
                            val info = wifiManager.getConnectionInfo();
                            newSSID  = info.getSSID()
                            currentWifissid.text = String.format(getString(R.string.current_ssid), newSSID)
                            if (oldSSID == ""){
                                oldWifissid.text = String.format(getString(R.string.old_ssid), newSSID)
                                currentWifissid.text = String.format(getString(R.string.current_ssid), newSSID)
                                oldSSID = newSSID
                            }
                            if (oldSSID != newSSID){
                                if (geofenceTransition == Geofence.GEOFENCE_TRANSITION_EXIT){
                                    status.text = "${getString(R.string.geofence_transition_outside)}  : $triggeringGeofencesIdString"
                                }
                            }
                        } else {
                            newSSID = ""
                        }
                    }

                }, {}, {})

        compositeDisposable.add(disposable)
    }

    private fun attachMapFragmentToView(){
        map.onCreate(null)
        map.getMapAsync(this)
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
        if (!checkPermissions()) {
            requestPermissions()
        } else {
            performPendingGeofenceTask()
        }
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onGeofenceEvent(event: GeofenceEvent) {
        /**
         * SSID is changed
         */
        geofenceTransition = event.geofenceTransition
        triggeringGeofencesIdString = event.triggeredIDString
        if (oldSSID == newSSID) {
            status.text = "${getString(R.string.geofence_transition_inside)}: ${event.triggeredIDString}"
        } else {
            val v = activity?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
            v.vibrate(VibrationEffect.createOneShot(300, VibrationEffect.DEFAULT_AMPLITUDE))
            status.text =  "${getTransitionString(event.geofenceTransition)}: ${event.triggeredIDString}"
        }
    }

    /**
     * Adds geofences. This method should be called after the user has granted the location
     * permission.
     */
    @SuppressWarnings("MissingPermission")
    private fun addGeofences() {
        if (!checkPermissions()) {
            showSnackbar(getString(R.string.insufficient_permissions));
            return
        }

        mGeofencingClient.addGeofences(presenter.getGeofencingRequest(), getGeofencePendingIntent())
                .addOnCompleteListener(this)
    }

    /**
     * Removes geofences. This method should be called after the user has granted the location
     * permission.
     */
    @SuppressWarnings("MissingPermission")
    private fun removeGeofences() {
        if (!checkPermissions()) {
            showSnackbar(getString(R.string.insufficient_permissions))
            return
        }

        mGeofencingClient.removeGeofences(getGeofencePendingIntent()).addOnCompleteListener(this);
    }

    /**
     * Runs when the result of calling {@link #addGeofences()} and/or {@link #removeGeofences()}
     * is available.
     * @param task the resulting Task, containing either a result or error.
     */
    override fun onComplete(task: Task<Void>) {
        mPendingGeofenceTask = PendingGeofenceTask.NONE
        if (task.isSuccessful()) {
            updateGeofencesAdded(!getGeofencesAdded());
        } else {
            // Get the status code for the error and log it using a user-friendly message.
            val errorMessage = GeofenceErrorMessages.getErrorString(activity as Context, task.getException());
            Log.w(TAG, errorMessage);
        }
    }

    /**
     * Gets a PendingIntent to send with the request to add or remove Geofences. Location Services
     * issues the Intent inside this PendingIntent whenever a geofence transition occurs for the
     * current list of geofences.
     *
     * @return A PendingIntent for the IntentService that handles geofence transitions.
     */
    private fun getGeofencePendingIntent(): PendingIntent? {
        // Reuse the PendingIntent if we already have it.
        if (mGeofencePendingIntent != null) {
            return mGeofencePendingIntent;
        }
        val intent = Intent(activity, GeofenceBroadcastReceiver::class.java)
        // We use FLAG_UPDATE_CURRENT so that we get the same pending intent back when calling
        // addGeofences() and removeGeofences().
        mGeofencePendingIntent = PendingIntent.getBroadcast(activity as Context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        return mGeofencePendingIntent;
    }

    /**
     * Shows a {@link Snackbar} using {@code text}.
     *
     * @param text The Snackbar text.
     */
    private fun showSnackbar(text: String) {
        val container = geoFenceView.findViewById<View>(android.R.id.content)
        if (container != null) {
            Snackbar.make(container, text, Snackbar.LENGTH_LONG).show()
        }
    }

    /**
     * Shows a {@link Snackbar}.
     *
     * @param mainTextStringId The id for the string resource for the Snackbar text.
     * @param actionStringId   The text of the action item.
     * @param listener         The listener associated with the Snackbar action.
     */
    private fun showSnackbar(mainTextStringId: Int, actionStringId: Int, listener: View.OnClickListener) {
        Snackbar.make(
                geoFenceView.findViewById(android.R.id.content),
                getString(mainTextStringId),
                Snackbar.LENGTH_INDEFINITE)
                .setAction(getString(actionStringId), listener).show();
    }

    /**
     * Returns true if geofences were added, otherwise false.
     */
    private fun getGeofencesAdded(): Boolean {
        return PreferenceManager.getDefaultSharedPreferences(activity as Context).getBoolean(
                Constants.GEOFENCES_ADDED_KEY, false);
    }

    /**
     * Stores whether geofences were added ore removed in {@link SharedPreferences};
     *
     * @param added Whether geofences were added or removed.
     */
    private fun updateGeofencesAdded(added: Boolean) {
        PreferenceManager.getDefaultSharedPreferences(activity as Context)
                .edit()
                .putBoolean(Constants.GEOFENCES_ADDED_KEY, added)
                .apply();
    }

    /**
     * Performs the geofencing task that was pending until location permission was granted.
     */
    private fun performPendingGeofenceTask() {
        if (mPendingGeofenceTask == PendingGeofenceTask.ADD) {
            addGeofences()
        } else if (mPendingGeofenceTask == PendingGeofenceTask.REMOVE) {
            removeGeofences()
        }
    }

    /**
     * Return the current state of the permissions needed.
     */
    private fun checkPermissions(): Boolean {
        val permissionState = checkSelfPermission(activity as Context,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private fun requestPermissions() {
        val shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(activity as Activity,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");
            showSnackbar(R.string.permission_rationale, android.R.string.ok,
                    object: View.OnClickListener {
                        override fun onClick(view: View?) {
                            // Request permission
                            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                                    REQUEST_PERMISSIONS_REQUEST_CODE)
                        }
                    });
        } else {
            Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION),
                    REQUEST_PERMISSIONS_REQUEST_CODE)
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.isEmpty()) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Permission granted.");
                initWifiAndMaps()
                performPendingGeofenceTask()
            } else {
                // Permission denied.

                // Notify the user via a SnackBar that they have rejected a core permission for the
                // app, which makes the Activity useless. In a real app, core permissions would
                // typically be best requested during a welcome-screen flow.

                // Additionally, it is important to remember that a permission might have been
                // rejected without asking the user for permission (device policy or "Never ask
                // again" prompts). Therefore, a user interface affordance is typically implemented
                // when permissions are denied. Otherwise, your app could appear unresponsive to
                // touches or interactions which have required permissions.
                showSnackbar(R.string.permission_denied_explanation, R.string.settings,(
                        View.OnClickListener {
                                // Build intent that displays the App settings screen.
                                val intent = Intent()
                                intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
                                val uri = Uri.fromParts("package", BuildConfig.APPLICATION_ID, null);
                                intent.data = uri
                                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                                startActivity(intent);
                        }))
                mPendingGeofenceTask = PendingGeofenceTask.NONE;
            }
        }
    }
    @SuppressWarnings("MissingPermission")
    override fun onMapReady(googleMap: GoogleMap?) {
        if (!checkPermissions()) {
            requestPermissions()
            return
        }
        mGeofencingClient = LocationServices.getGeofencingClient(activity as Activity)
        val locationManager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val criteria = Criteria();
        val currentLocation = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));

        presenter.initGeofence(mGeofencingClient, currentLocation)
        initGoogleMap(googleMap, currentLocation)
    }

    @SuppressWarnings("MissingPermission")
    private fun initGoogleMap(googleMap: GoogleMap?, currentLocation: Location){
        if (googleMap == null) return

        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(currentLocation.latitude, currentLocation.longitude), 19f));
        googleMap.getUiSettings().setMyLocationButtonEnabled(false);
        googleMap.isMyLocationEnabled = true

        map.onResume()
        mMap = googleMap
        val circleOptions = CircleOptions()
                .center(LatLng(currentLocation.latitude + Constants.GEOFENCE_DISTANCE, currentLocation.longitude + Constants.GEOFENCE_DISTANCE))
                .radius(Constants.GEOFENCE_RADIUS_IN_METERS.toDouble())
                .fillColor(0x40ff0000)
                .strokeColor(Color.TRANSPARENT)
                .strokeWidth(2f)
        circle = mMap.addCircle(circleOptions)

        if (circle != null) {
            seekbar.onProgressChangedListener = object : BubbleSeekBar.OnProgressChangedListener {
                override fun onProgressChanged(bubbleSeekBar: BubbleSeekBar?, progress: Int, progressFloat: Float, fromUser: Boolean) {
                    circle!!.radius = progress.toDouble()
                }

                override fun getProgressOnActionUp(bubbleSeekBar: BubbleSeekBar?, progress: Int, progressFloat: Float) {
                    triggerGeofence()
                }

                override fun getProgressOnFinally(bubbleSeekBar: BubbleSeekBar?, progress: Int, progressFloat: Float, fromUser: Boolean) {

                }
            }
        }

    }

    private fun triggerGeofence(){
        if (oldSSID == newSSID){
            status.text = "${getString(R.string.geofence_transition_inside)}: $triggeringGeofencesIdString"
        } else if (oldSSID != newSSID){
            status.text = "${getString(R.string.geofence_transition_outside)}: $triggeringGeofencesIdString"
        }

        presenter.populateGeofenceList(circle!!.radius.toFloat())
        mPendingGeofenceTask = PendingGeofenceTask.ADD
        removeGeofences()
        addGeofences()
    }

    private fun getTransitionString(transitionType: Int): String {
        when (transitionType) {
            Geofence.GEOFENCE_TRANSITION_ENTER -> return getString(R.string.geofence_transition_inside)
            Geofence.GEOFENCE_TRANSITION_EXIT -> return getString(R.string.geofence_transition_outside)
            else -> return getString(R.string.unknown_geofence_transition)
        }
    }
}