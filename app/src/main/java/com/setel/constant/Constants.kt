package com.setel.constant

import com.google.android.gms.maps.model.LatLng

class Constants {
    companion object{
        private val PACKAGE_NAME = "com.google.android.gms.location.Geofence"

        val GEOFENCES_ADDED_KEY = "$PACKAGE_NAME.GEOFENCES_ADDED_KEY"

        /**
         * Used to set an expiration time for a geofence. After this amount of time Location Services
         * stops tracking the geofence.
         */
        private val GEOFENCE_EXPIRATION_IN_HOURS: Long = 12

        /**
         * For this sample, geofences expire after twelve hours.
         */
        val GEOFENCE_EXPIRATION_IN_MILLISECONDS =
                GEOFENCE_EXPIRATION_IN_HOURS * 60 * 60 * 1000
        val GEOFENCE_RADIUS_IN_METERS = 10f // 1 mile, 1.6 km

        /**
         * Map for storing information about airports in the San Francisco bay area.
         */
        val BAY_AREA_LANDMARKS = hashMapOf("Setel" to LatLng(3.111744, 101.667969))

        val GEOFENCE_DISTANCE = 0.0001
    }
}