package com.setel.model

data class GeofenceEvent(var triggeredIDString: String = "", var geofenceTransition: Int = -1)