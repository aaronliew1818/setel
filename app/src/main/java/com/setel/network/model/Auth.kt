package com.setel.network.model

/**
 * Created by aaronliew on 05/03/2018.
 */
data class Auth(var access_token: String,
                var token_type: String,
                var expires_in: String,
                var token_expiresUtc: String)