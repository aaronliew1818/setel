package com.setel.network.model

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by aaronliew on 05/03/2018.
 */
data class TransactionDetail(var TransactionId: Int,
                             private var CreatedDate: String,
                             var SenderAmount: Double,
                             var RecipientCountry: String,
                             var RecipientCurrency: String,
                             var Status: Int,
                             var RecipientInfo: Recipientinfo) {

    data class Recipientinfo(var Name: String,
                             var Relationship: String,
                             var ContactNumber: String)


    fun getCreatedDate(): String {

        try {
            val parsedFormat =  SimpleDateFormat(
                    "yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
            parsedFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            val date = parsedFormat.parse(CreatedDate);

            val outFormat = SimpleDateFormat("h:mma dd/MM/yy", Locale.ENGLISH);
            return outFormat.format(date);
        } catch (e : ParseException){
            return CreatedDate;
        }

    }

    fun getRecipientName() : String = RecipientInfo.Name;

    fun  getRecipientRelationShip() : String =  RecipientInfo.Relationship;

    fun getRecipientContactNumber() : String = RecipientInfo.ContactNumber;

    fun getSenderAmount() : String = SenderAmount.toString();
}