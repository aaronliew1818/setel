package com.setel.network

import android.content.SharedPreferences
import com.setel.Const
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by aaronliew on 05/03/2018.
 */
class RetrofitInterceptor(val sharedPreferences : SharedPreferences) : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val token = sharedPreferences.getString(Const.ACCESS_TOKEN, "");
        val request = chain.request();
        val authenticatedRequest = request.newBuilder()
                .header("Authorization", "Bearer "+ token).build();
        return chain.proceed(authenticatedRequest);
    }
}