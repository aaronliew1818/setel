package com.setel

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.setel.di.Qualifiers
import com.setel.di.component.AppComponent
import com.setel.di.component.DaggerAppComponent
import com.setel.di.module.AppModule
import com.setel.di.module.NetworkModule
import javax.inject.Inject

/**
 * Created by aaronliew on 04/03/2018.
 */
class BaseApplication : Application() {
    @Inject
    @field:Qualifiers.SharedPreferences
    lateinit var mSharedPreferences : SharedPreferences;

    companion object {
        fun get(context: Context) : BaseApplication = context.applicationContext as BaseApplication;

        @JvmStatic lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
                .networkModule(NetworkModule())
                .appModule(AppModule(this))
                .build();

        appComponent.inject(this);

        val editor = mSharedPreferences.edit();
        editor.putString("name", "Elena");
        editor.apply();
    }

    fun  getComponent() : AppComponent = appComponent;

}