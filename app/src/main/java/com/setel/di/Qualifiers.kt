package com.setel.di

import java.lang.annotation.RetentionPolicy
import javax.inject.Qualifier

/**
 * Created by aaronliew on 04/03/2018.
 */
interface Qualifiers {
    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class SharedPreferences {}

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class Retrofit {}

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class SetelWebService {}
}