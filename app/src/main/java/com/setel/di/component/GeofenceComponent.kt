package com.setel.di.component

import com.setel.di.PerFragment
import com.setel.di.module.GeoFenceModule
import com.setel.ui.geofence.GeofenceFragment
import dagger.Component

/**
 * Created by aaronliew on 04/03/2018.
 */
@PerFragment
@Component(dependencies = [AppComponent:: class] , modules = [GeoFenceModule:: class])
interface GeofenceComponent {
    fun inject(target : GeofenceFragment)
}