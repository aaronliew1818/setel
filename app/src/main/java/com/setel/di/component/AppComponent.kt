package com.setel.di.component

import android.content.SharedPreferences
import com.setel.BaseApplication
import com.setel.di.Qualifiers
import com.setel.di.module.AppModule
import com.setel.di.module.NetworkModule
import com.setel.network.SetelWebService
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by aaronliew on 04/03/2018.
 */
@Singleton
@Component(modules = arrayOf(AppModule::class, NetworkModule::class))
interface AppComponent {
    @Qualifiers.Retrofit
    fun retrofit() : Retrofit ;

    @Qualifiers.SharedPreferences
    fun sharedPreferences(): SharedPreferences

    @Qualifiers.SetelWebService
    fun setelWebService() : SetelWebService

    fun inject(application: BaseApplication)
}