package com.setel.di;

/**
 * Created by bot on 18/01/2018.
 */

import java.lang.annotation.RetentionPolicy;
import javax.inject.Qualifier

import javax.inject.Scope;

@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerFragment {}