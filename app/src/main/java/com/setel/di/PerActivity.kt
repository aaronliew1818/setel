package com.setel.di

import javax.inject.Scope

/**
 * Created by aaronliew on 04/03/2018.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerActivity {}