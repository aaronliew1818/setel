package com.setel.di.module

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import com.setel.Const
import com.setel.di.Qualifiers
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by aaronliew on 04/03/2018.
 */
@Module
class AppModule(var mApplication: Application) {

    @Singleton
    @Provides
    @Qualifiers.SharedPreferences
    fun provideSharedPreferences() : SharedPreferences = mApplication
            .getSharedPreferences(Const.SHARED_PREF_NAME, Context.MODE_PRIVATE);

}