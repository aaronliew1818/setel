package com.setel.di.module;

import android.content.SharedPreferences;

import com.setel.BuildConfig;
import com.setel.di.Qualifiers;
import com.setel.network.RetrofitInterceptor;
import com.setel.network.SetelWebService;

import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by bot on 19/01/2018.
 */
@Module
class NetworkModule {

    companion object {
        val CONNECT_TIMEOUT_IN_MS = 30000;
    }

    @Provides
    @Singleton
    fun provideOkHttpClient(@Qualifiers.SharedPreferences sharedPreferences : SharedPreferences) : OkHttpClient {
        return  OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT_IN_MS.toLong(), TimeUnit.MILLISECONDS)
                .addInterceptor(RetrofitInterceptor(sharedPreferences))
                .build()
    }

    @Singleton
    @Provides
    @Qualifiers.Retrofit
    fun retrofit(okHttpClient : OkHttpClient) : Retrofit = Retrofit
                .Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()


    @Singleton
    @Provides
    @Qualifiers.SetelWebService
    fun provideSetelWebService(@Qualifiers.Retrofit retrofit: Retrofit) : SetelWebService {
        return retrofit.create(SetelWebService::class.java);
    }

}
