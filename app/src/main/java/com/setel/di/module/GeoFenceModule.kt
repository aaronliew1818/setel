package com.setel.di.module

import com.setel.ui.geofence.GeoFenceViewPresenter
import com.setel.ui.geofence.GeoFenceViewPresenterImpl
import dagger.Module
import dagger.Provides

/**
 * Created by aaronliew on 05/03/2018.
 */
@Module
class GeoFenceModule {
    @Provides
    fun provideGeoFenceViewPresenter(): GeoFenceViewPresenter = GeoFenceViewPresenterImpl()
}