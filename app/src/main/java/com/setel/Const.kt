package com.setel;

/**
 * Created by bot on 27/01/2018.
 */

class  Const {
    //Enum : https://developer.android.com/topic/performance/memory.html#Overhead
    companion object {
        const val SHARED_PREF_NAME = "setel-prefs";
        const val ACCESS_TOKEN = "access_token";

        const val PENDING_STATUS_CODE = 1;
        const val SUCCESS_STATUS_CODE = 2;
        const val FAIL_STATUS_CODE = 3;
    }

}
