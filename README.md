# Features
## Google Map
Allow user to view:
1) location of current user. 
2) location and radius of geofence

## Seekbar
Allow user to change the radius of the geofence.

## Prev & Current WiFi SSiD
Allow user to compare the prev and current Wifi SSID to ease the testing of the app.

## Explanation on how the app works
1) If there is WiFi changes but device is inside the range of the geofence, it displays "Inside"
2) If there is WiFi changes but device is outside the range of the geofence, it displays "Outside"
3) If there is no WiFi changes but device is outside the range of the geofence, it displays "Inside"
4) If there is no WiFi changes but device is outside the range of the geofence, it displays "Inside"

I would like to recommend this app to do mock the location. Here is link:
https://play.google.com/store/apps/details?id=com.theappninjas.fakegpsjoystick&hl=en

Thanks. Let me know if you have any questions



